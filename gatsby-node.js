const path = require('path')
const axios = require('axios');
const crypto = require('crypto');
const fetch = require('node-fetch');

exports.createPages = async ({ graphql, actions }) => {
    
    const { data } = await graphql(`
        query Projects {
            allMarkdownRemark {
                nodes {
                    frontmatter {
                        slug
                    }
                }
            }
        }
    `)

    data.allMarkdownRemark.nodes.forEach(node => {
        actions.createPage({
            path: '/projects/' + node.frontmatter.slug,
            component: path.resolve('./src/templates/project-details.js'),
            context: { slug: node.frontmatter.slug }
        })
    })

}

exports.sourceNodes = async ({ actions }) => {
    const { createNode } = actions;
    const endpoint = "https://thankful-spider-36.hasura.app/v1/graphql";
    const headers = {
        "content-type": "application/json",
        "x-hasura-admin-secret": "ylp8d4spMC4vN9V6NNeM4Quh3BrmtarqOQEGtLzKUJpvfu6JBlSfW4GkcDwy01uB"
    };
    const graphqlQuery = {
        "operationName": "getUser",
        "query": `query getUser{
            users{
              id
              name
            }
        }`,
        "variables": {}
    };
    
    const options = {
        "method": "POST",
        "headers": headers,
        "body": JSON.stringify(graphqlQuery)
    };
    
    const response = await fetch(endpoint, options);
    const res = await response.json();

    const users = [];
    res.data.users.map((user, i) => {
        users.push(user);
    });
    const userNode = {
        id: crypto.createHash(`md5`).update(JSON.stringify(res.data.users)).digest(`hex`),
        internal: {
            type: `getUser`,
        },
        users: users
    }
    userNode.internal.contentDigest = crypto.createHash(`md5`).update(JSON.stringify(userNode)).digest(`hex`);
    createNode(userNode);
    
    return;
}
